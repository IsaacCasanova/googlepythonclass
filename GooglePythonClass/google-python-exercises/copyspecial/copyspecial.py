#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands


"""Copy Special exercise"""


# +++your code here+++
# Write functions and modify main() to call them

# returns a list of absolute file paths of the special files
# in the given directory
def get_special_paths(dir):
    special_paths = []
    file_names = os.listdir(dir)
    for file_name in file_names:
        match = re.search(r'__(\w+)__', file_name)
        if match:
            file_location = os.path.join(dir, file_name)
            absolute_path = os.path.abspath(file_location)
            special_paths.append(absolute_path)
    return special_paths


# given a list of paths, copies those files into the given directory
def copy_to(paths, dir):
    validate_paths(paths)

    if not os.path.exists(dir):
        os.mkdir(dir)
        print("made directory", dir)

    for path in paths:
        shutil.copy(path, dir)


def zip_to(paths, zippath):
    validate_paths(paths)
    pathstr = " "

    for path in paths:
        pathstr += str(path) + " "

    cmd = "zip -j" + " " + zippath + pathstr
    print("Command I'm going to do:" + cmd)
    (status, output) = commands.getstatusoutput(cmd)

    # Error case, print the command's output to stderr and exit
    if status:
        sys.stderr.write(output)
        sys.exit(1)


def validate_paths(paths):
    if len(paths) == 0:
        print("list is empty! Aborting!")
        sys.exit(1)


def main():
    # This basic command line argument parsing code is provided.
    # Add code to call your functions below.

    # Make a list of command line arguments, omitting the [0] element
    # which is the script itself.
    args = sys.argv[1:]
    if not args:
        print("usage: [--todir dir][--tozip zipfile] dir [dir ...]")
        sys.exit(1)

    # todir and tozip are either set from command line
    # or left as the empty string.
    # The args array is left just containing the dirs.

    # +++your code here+++
    # Call your functions
    todir = ''
    from_dir = ''
    if args[0] == '--todir':
        if len(args) < 3:
            print("usage: [--todir dir] dir [dir ...]")
            sys.exit(1)

        todir = args[1]
        from_dir = args[2]

        special_paths = get_special_paths(from_dir)
        copy_to(special_paths, todir)

        del args[0:2]

    tozip = ''
    if args[0] == '--tozip':
        if len(args) < 3:
            print("usage: [--tozip zipfile] dir [dir ...]")
            sys.exit(1)

        tozip = args[1]
        from_dir = args[2]

        special_paths = get_special_paths(from_dir)
        zip_to(special_paths, tozip)

        del args[0:2]

    if len(args) == 0:
        print("error: must specify one or more dirs")
        sys.exit(1)

    if len(todir) == 0 and len(tozip) == 0:
        for special_path in get_special_paths(args[0]):
            print(special_path)


if __name__ == "__main__":
    main()
