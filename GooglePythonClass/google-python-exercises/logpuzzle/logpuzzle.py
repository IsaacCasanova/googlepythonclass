#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import os
import re
import sys
import urllib

"""Logpuzzle exercise
Given an apache logfile, find the puzzle urls and download the images.

Here's what a puzzle url looks like:
10.254.254.28 - - [06/Aug/2007:00:13:48 -0700]
"GET /~foo/puzzle-bar-aaab.jpg HTTP/1.0" 302 528
"-" "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6)
Gecko/20070725 Firefox/2.0.0.6"
"""

"""Returns a list of the puzzle urls from the given log file,
extracting the hostname from the filename itself.
Screens out duplicate urls and returns the urls sorted into
increasing order."""


def read_urls(filename):
    # +++your code here+++
    server_name_match = re.findall(r'_(.*)', filename)
    if not server_name_match:
        print('host must be preceeded by _' + '\n')
        sys.exit(1)

    try:
        f = open(filename, 'rU')
        file_text = f.read()
        f.close()
    except IOError:
        sys.stderr.write('problem reading: ' + filename + '\n')
        sys.exit(1)

    url_scheme = 'http://'
    server_name = server_name_match[0]
    url_host = url_scheme + server_name

    # extract puzzle urls
    puzzle_urls = re.findall(r'GET\s(\S+)\s', file_text)

    # construct full urls
    img_urls = [url_host + url for url in puzzle_urls
                if 'puzzle' in url]

    # attempt to extract '-wordchars-(wordchars).jpg'
    wordchar_url_match = re.search(r'-(\w+)-(\w+).jpg', img_urls[0])
    if wordchar_url_match:
        return sorted_wordchar_urls(img_urls)

    # extract distinct_urls
    distinct_urls = list(set(img_urls))

    # sort urls alphabetically
    sorted_urls = sorted(distinct_urls)

    return sorted_urls


def sorted_wordchar_urls(puzzle_urls):
    copy_puzzle_urls = puzzle_urls
    distinct_wordchar_urls = list(set(copy_puzzle_urls))

    return sorted(distinct_wordchar_urls, key=sort_by_second_wordchar)


def sort_by_second_wordchar(s):
    return re.search(r'-(\w+)-(\w+).jpg', s).group(2)


"""Given the urls already in the correct order, downloads
each image into the given directory.
Gives the images local filenames img0, img1, and so on.
Creates an index.html in the directory
with an img tag to show each local image file.
Creates the directory if necessary."""


def download_images(img_urls, dest_dir):
    # +++your code here+++
    if not os.path.exists(dest_dir):
        os.mkdir(dest_dir)
        print('made directory ', dest_dir)

    # append '/' for easier string building
    dest_dir += '/'

    # download url data to directory provided
    for i in range(len(img_urls)):
        url = img_urls[i]
        file_name = dest_dir + 'img' + str(i)
        try:
            print('retrieving data from url: ' + url)
            urllib.urlretrieve(url, file_name)
        except IOError:
            print('problem retrieving data for url: ' + url)

    # create index.html file for viewing downloaded data
    img_str = ''
    for i in range(len(img_urls)):
        file_name = dest_dir + 'img' + str(i)
        abs_file_name = os.path.abspath(file_name)
        img_str += str.format('<img src="%s">' % abs_file_name)

    html_str = str.format('<html><body>%s</body></html>' % img_str)

    try:
        f = open(dest_dir + 'index.html', 'w')
        f.write(html_str)
        f.close()
    except IOError:
        sys.stderr.write('problem creating file index.html')
        sys.exit(1)


def main():
    args = sys.argv[1:]

    if not args:
        print('usage: [--todir dir] logfile')
        sys.exit(1)

    todir = ''
    if args[0] == '--todir':
        todir = args[1]
        del args[0:2]

    img_urls = read_urls(args[0])

    if todir:
        download_images(img_urls, todir)
    else:
        print('\n'.join(img_urls))


if __name__ == '__main__':
    main()
